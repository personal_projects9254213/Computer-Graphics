#version 430 core                                                    
out vec4 fColor;   

in vec3 fNorm;
in vec3 fPos;
in vec2 uv;

uniform sampler2D AlbedoMap;
uniform sampler2D MetallicMap;
uniform sampler2D RoughMap;
uniform sampler2D NormalMap;
uniform sampler2D AoMap;

uniform samplerCube skybox;

uniform mat4 W;

uniform float mode;

struct Light
{
	vec3 Position;		
	vec3 Ls;		
};

uniform Light light[4];

uniform vec3 camPos;
        
const float PI = 3.14159265359;

float distributionGGX(float NdotH, float roughness)
{
    float a = roughness * roughness;
    float a2     = a*a;
    float NdotH2 = NdotH*NdotH;
	
    float nom    = a2;
    float denom  = (NdotH2 * (a2 - 1.0) + 1.0);
    denom        = PI * denom * denom;
	
    return nom / denom;
}
float geometrySchlickGGX(float NdotV, float k)
{
    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;
	
    return nom / denom;
}
  
float geometrySmith(float NdotV, float NdotL, float roughness)
{
    float r = roughness + 1.0;
    float k = (r*r)/8.0;
    float ggx1 = geometrySchlickGGX(NdotV, k);
    float ggx2 = geometrySchlickGGX(NdotL, k);
	
    return ggx1 * ggx2;
}

vec3 fresnelSchlick(float cosTheta, vec3 reflectivity)
{
    return reflectivity + (1.0 - reflectivity) * pow(1.0 - cosTheta, 5.0);
}
/////////////////////////////////////////////////////////////////

vec3 fresnelSchlickRoughness(float cosTheta, vec3 F0, float roughness)
{
    return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
} 

vec3 pow(vec3 v, float alpha){
    float x = pow(v.x, alpha);
    float y = pow(v.y, alpha);
    float z = pow(v.z, alpha);

    return vec3(x,y,z);
}
void main(void)                                                   
{    
    // Get values from textures
    vec3 albedo = pow(texture(AlbedoMap,uv).rgb,2.2); //pow(texture(albedoMap, TexCoords).rgb, 2.2);
    float metallic = texture(MetallicMap,uv).r;
    float roughness = texture(RoughMap,uv).r;
    float ao = texture(AoMap,uv).r;
    vec3 N = (texture(NormalMap,uv).rgb);
    N = (normalize(N*2.0-1.0));

    vec3 V = normalize(camPos - fPos);
    
    float NdotV = max(dot(N,V), 0.000001); //cosLo
    //Fresnel reflectance
    vec3 reflectivity = mix(vec3(0.04), albedo, metallic);

    vec3 Lo = vec3(0.0f); //direct light
    
    for (int i=0;i<4;i++){

        vec3 L = normalize(light[i].Position - fPos);
        vec3 H = normalize(V + L);


        float NdotH = max(dot(N,H), 0.0); //cosLh
        float NdotL = max(dot(N,L), 0.000001); //cosLi
    
        float HdotV = max(dot(H,V), 0.0);
        float LdotN = max(dot(L,N), 0.000001);

        vec3 F = fresnelSchlick(HdotV, reflectivity);
        float D = distributionGGX(NdotH, roughness);
        float G = geometrySmith(NdotV, NdotL, roughness);

        vec3 specular = D*G*F;
        specular /= 4.0 * NdotV * NdotL;

        vec3 kD =  mix(vec3(1.0) - F, vec3(0.0), metallic);

        float distance = length(light[i].Position - fPos);
        float attenuation = 1.0 / (distance*distance);
        vec3 radiance = light[i].Ls * attenuation;

        Lo += (kD*albedo + specular) * radiance * NdotL;

    }

    vec3 I = normalize(fPos - camPos);
    vec3 R = reflect(I,  normalize(mat3(transpose(inverse(W))) *(N)));
    vec3 worldRefl = R; //
    vec3 amb_clr = texture(skybox, worldRefl).rgb;

    vec3 F = fresnelSchlickRoughness(max(dot(N, V), 0.0), reflectivity, roughness);
    
    vec3 kS = F;
    vec3 kD = 1.0 - kS;
   
    vec3 ambient = (kD*amb_clr*albedo) * ao;    
    
    vec3 color = ambient + Lo;

    color = color / (color + vec3(1.0));
    color = pow(color, vec3(1.0/2.2));

    fColor = vec4(color,1.0);
    if(mode==2.0f){
        fColor = vec4(texture(AlbedoMap,uv).rgb,1.0);
    }
    else if(mode==3.0f){
        fColor = vec4(texture(MetallicMap,uv).r,0.0,0.0,1.0);
    }
    else if(mode==4.0f){
        fColor = vec4(texture(RoughMap,uv).r,0.0,0.0,1.0); 
    }
    else if(mode==5.0f){
        fColor = vec4(texture(AoMap,uv).rgb,1.0);  
    }
    else if(mode==6.0f){
        fColor = vec4(texture(NormalMap,uv).rgb,1.0);
    }

} 


