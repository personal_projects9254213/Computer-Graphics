#version 430 core                                                 
in vec3 vPos;                                                    
in vec3 vNorm;    
in vec2 vTex;


uniform mat4 PM;   
uniform mat4 PV;   
uniform mat4 M;
uniform mat4 W;

out vec3 fPos;
out vec3 fNorm;
out vec2 uv;

uniform sampler2D NormalMap;
 

void main(void)                                                   
{                                                                 
    vec4 position = W * vec4(vPos, 1.0);
    
    fPos = position.xyz / position.w;
    gl_Position =  PM * vec4(vPos, 1.0f);  
    //fNorm = mat3(transpose(inverse(W)))*vNorm;
    //fPos = vec3(W * vec4(vPos, 1.0));
    uv = vec2(vTex.x, vTex.y);    
   
    
} 


