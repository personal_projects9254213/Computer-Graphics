#version 430 core
layout (location = 0) in vec3 aPos;

out vec3 TexCoords;

uniform mat4 position;
uniform mat4 view;
out vec3 WorPos;
void main()
{
    TexCoords = aPos;
    vec4 pos = position * view * vec4(aPos, 1.0);
    gl_Position = pos.xyww;
} 