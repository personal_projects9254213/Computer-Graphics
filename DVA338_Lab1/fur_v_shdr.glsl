#version 420 core                                                 
                                                                 
in vec3 vPos;                                                    
in vec3 vNorm;

uniform mat4 W; 

struct Material
{
	vec3 Ka;			
	vec3 Kd;			
	vec3 Ks;			
	float Shininess;	
};   

uniform Material material;

out Vertex
{
  vec4 normal;
  vec4 color;

} vertex;
                                                                  
void main(void)                                                   
{                                                                   
    vertex.normal = vec4(vNorm,1.0f);
    gl_Position = vec4(vPos, 1.0f);//                          
}  