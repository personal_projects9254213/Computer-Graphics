#version 420 core                                                 
                   
layout(triangles) in;

layout(line_strip, max_vertices=256) out;

float MAGNITUDE = 0.02;
uniform mat4 PM;

//in vec4 normal[];


in Vertex
{
  vec4 normal;
  vec4 color;
} vertex[];

out vec4 vertex_color;

float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main(){

  int i;
 //for(i=0; i<gl_in.length(); i++)
 //{
 //  vec3 P = gl_in[i].gl_Position.xyz;
 //  vec3 N = vertex[i].normal.xyz;
 //  
 //  gl_Position = PM * vec4(P, 1.0);
 //  vertex_color = vec4(0.5,0.2,0.2,1.0);
 //  EmitVertex();
 //  
 //  gl_Position = PM * vec4(P + N * MAGNITUDE, 1.0);
 //  vertex_color = vec4(0.5,0.2,0.2,1.0);
 //  EmitVertex();
 //  
 //  EndPrimitive();
 //  }

  for(i=0; i<gl_in.length()-2; i++)
  {
        vec3 P0 = gl_in[i].gl_Position.xyz;
        vec3 P1 = gl_in[i+1].gl_Position.xyz;
        vec3 P2 = gl_in[i+2].gl_Position.xyz;

        vec3 N0 = vertex[i].normal.xyz;
        vec3 N1 = vertex[i+1].normal.xyz;
        vec3 N2 = vertex[i+2].normal.xyz;
        
        vec3 V0 = P0 - P1;
        vec3 V1 = P2 - P1;
        
        vec3 N = cross(V1, V0);
        N = normalize(N);
        
        // Center of the triangle
        vec3 P = (P0+P1+P2) / 3.0;
        
        gl_Position = PM * vec4(P, 1.0);
        vertex_color =  vec4(0.5,0.2,0.2,1.0);
        EmitVertex();
        
        gl_Position = PM * vec4(P + N * MAGNITUDE, 1.0);
        vertex_color =  vec4(0.5,0.2,0.2,1.0);
        EmitVertex();
        EndPrimitive();

        float a,b,c;
        int j,k;
        float x = rand(vec2(0.478355,0.823912));
        float y = rand(vec2(0.322947,0.502290));
        for (j=0;j<256;j++){
            a= x*0.5;
            b= 1.0-a;
            c = b*y;
            b = b-c;
            vec3 P ;//= (P0*a+P1*b+P2*c);
            vec3 N ;//= normalize(N0*a+N1*b+N2*c);
            if(j%6==0){
                P = (P0*a+P1*b+P2*c);   // a  + b +c = 1
                N = normalize(N0*a+N1*b+N2*c);
            } 
            else if(j%6==1){
                P = (P1*a+P2*b+P0*c);
                N = normalize(N1*a + N2*b + N0*c);
            } 
            else if(j%6==2){
                P = (P2*a+P0*b+P1*c);
                N = normalize(N1*a + N2*b + N0*c);
            }
            else if(j%6==3){
                P = (P0*a+P2*b+P1*c);
                N = normalize(N0*a + N2*b + N1*c);
            } 
            else if(j%6==4){
                P = (P2*a+P1*b+P0*c);
                N = normalize(N2*a + N1*b + N0*c);
            }
            else{
                P = (P1*a+P0*b+P2*c);
                N = normalize(N1*a + N0*b + N2*c);
            }


        
            gl_Position = PM * vec4(P, 1.0);
            vertex_color = vec4(0.5,0.2,0.2,1.0);
            EmitVertex();
        
            gl_Position = PM * vec4(P + N * MAGNITUDE, 1.0);
            vertex_color = vec4(0.5,0.2,0.2,1.0);
            EmitVertex();
            EndPrimitive();
            x = rand(vec2(x,y));
            y = rand(vec2(x,y));
            y = rand(vec2(y,x));
        }
    }
}


