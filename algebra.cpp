#define _USE_MATH_DEFINES // To get M_PI defined
#include <math.h>
#include <stdio.h>
#include "algebra.h"
#include "mesh.h"
double cotan(double alp) { return cos(alp)/sin(alp); }

Vector CrossProduct(Vector a, Vector b) {
	Vector v = { a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x };
	return v;
}

float DotProduct(Vector a, Vector b) {
	return a.x*b.x + a.y*b.y + a.z*b.z;
}

Vector Subtract(Vector a, Vector b) {
	Vector v = { a.x-b.x, a.y-b.y, a.z-b.z };
	return v;
}    

Vector Add(Vector a, Vector b) {
	Vector v = { a.x+b.x, a.y+b.y, a.z+b.z };
	return v;
}    

float Length(Vector a) {
	return sqrt(a.x*a.x + a.y*a.y + a.z*a.z);
}

Vector Normalize(Vector a) {
	float len = Length(a);
	Vector v = { a.x/len, a.y/len, a.z/len };
	return v;
}

Vector ScalarVecMul(float t, Vector a) {
	Vector b = { t*a.x, t*a.y, t*a.z };
	return b;
}

HomVector MatVecMul(Matrix a, Vector b) {
	HomVector h;
	h.x = b.x*a.e[0] + b.y*a.e[4] + b.z*a.e[8] + a.e[12];
	h.y = b.x*a.e[1] + b.y*a.e[5] + b.z*a.e[9] + a.e[13];
	h.z = b.x*a.e[2] + b.y*a.e[6] + b.z*a.e[10] + a.e[14];
	h.w = b.x*a.e[3] + b.y*a.e[7] + b.z*a.e[11] + a.e[15];
	return h;
}

Vector Homogenize(HomVector h) {
	Vector a;
	if (h.w == 0.0) {
		fprintf(stderr, "Homogenize: w = 0\n");
		a.x = a.y = a.z = 9999999;
		return a;
	}
	a.x = h.x / h.w;
	a.y = h.y / h.w;
	a.z = h.z / h.w;
	return a;
}

Matrix MatMatMul(Matrix a, Matrix b) {
	Matrix c;
	int i, j, k;
	for (i = 0; i < 4; i++) {
		for (j = 0; j < 4; j++) {
			c.e[j*4+i] = 0.0;
			for (k = 0; k < 4; k++)
				c.e[j*4+i] += a.e[k*4+i] * b.e[j*4+k];
		}
	}
	return c;
}

Matrix LookAt(const Vector eye, const Vector center, const Vector up) {
	Vector f = Normalize(Subtract(eye,center));
	Vector u = Normalize(up);
	Vector s = Normalize(CrossProduct(f, u));
	u = CrossProduct(s, f);
	Matrix result;
	result.e[0] = s.x;
	result.e[1] = s.y;
	result.e[2] = s.z;
	result.e[4] = u.x;
	result.e[5] = u.y;
	result.e[6] = u.z;
	result.e[8] = -f.x;
	result.e[9] = -f.y;
	result.e[10] = -f.z;
	result.e[12] = -DotProduct(s, eye);
	result.e[13] = -DotProduct(u, eye);
	result.e[14] = DotProduct(f, eye);
	result.e[15] = 1.0;

	return result;
}

/*
	Set unit matrix
*/
void unitMat(Matrix &m) {
	int i, j;
	for (i = 0; i < 4; i++)
		for (j = 0; j < 4; j++)
			if (i != j)
				m.e[j * 4 + i] = 0.0f;
			else
				m.e[j * 4 + i] = 1.0f;
}

void PrintVector(char *name, Vector a) {
	printf("%s: %6.5lf %6.5lf %6.5lf\n", name, a.x, a.y, a.z);
}

void PrintHomVector(char *name, HomVector a) {
	printf("%s: %6.5lf %6.5lf %6.5lf %6.5lf\n", name, a.x, a.y, a.z, a.w);
}

void PrintMatrix(char *name, Matrix a) { 
	int i,j;

	printf("%s:\n", name);
	for (i = 0; i < 4; i++) {
		for (j = 0; j < 4; j++) {
			printf("%6.5lf ", a.e[j*4+i]);
		}
		printf("\n");
	}
}

void matOrtho(Matrix& m, float scale, float left, float right, float bottom, float top, float near, float far) {
	unitMat(m);

	m.e[0] = scale / (right - left);
	m.e[5] = scale / (top - bottom);
	m.e[10] = scale / (near - far);

	m.e[12] = -((right + left) / (right - left));
	m.e[13] = -((top + bottom) / (top - bottom));
	m.e[14] = -((far + near) / (far - near));
}
void matPersp(Matrix& m, float fov, float aspect, float near, float far) {
	unitMat(m);
	fov = toRadian(fov);
	m.e[0] = cotan(fov/2.0) / aspect;
	m.e[5] = cotan(fov/2.0);
	m.e[10] = (far + near) / (near - far);
	m.e[11] = -1;
	m.e[14] = 2.0 * far * near / (near - far);
	m.e[15] = 0;

}

