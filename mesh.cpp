#define _CRT_SECURE_NO_WARNINGS
#include <stdlib.h>
#include "mesh.h"
#include <math.h>
#include <filesystem>
#include <string>
#include <iostream>
#include <fstream>
#include <windows.h>
using namespace std;

float toRadian(float x) { return x * 3.1415927 / 180.0; }

//njihov insert model
/*
void insertModel(Mesh** list, int nv, float* vArr, int nt, int* tArr, float scale) {
	Mesh* mesh = (Mesh*)malloc(sizeof(Mesh));
	mesh->nv = nv;
	mesh->nt = nt;
	mesh->vertices = (Vector*)malloc(nv * sizeof(Vector));
	mesh->vnorms = (Vector*)malloc(nv * sizeof(Vector));
	mesh->triangles = (Triangle*)malloc(nt * sizeof(Triangle));
	// set mesh vertices
	for (int i = 0; i < nv; i++) {
		mesh->vertices[i].x = vArr[i * 3] * scale;
		mesh->vertices[i].y = vArr[i * 3 + 1] * scale;
		mesh->vertices[i].z = vArr[i * 3 + 2] * scale;
	}

	// set mesh triangles
	for (int i = 0; i < nt; i++) {
		mesh->triangles[i].vInds[0] = tArr[i * 3];
		mesh->triangles[i].vInds[1] = tArr[i * 3 + 1];
		mesh->triangles[i].vInds[2] = tArr[i * 3 + 2];
	}

	// Assignment 1: 
	// Calculate and store suitable vertex normals for the mesh here.
	// Replace the code below that simply sets some arbitrary normal values	

	int ind_v0, ind_v1, ind_v2;
	Vector a, b, c;
	Vector u, p;
	Vector result;

	for (int i = 0; i < nt; i++) {  /// <------ nt, not nv
		ind_v0 = mesh->triangles[i].vInds[0];
		ind_v1 = mesh->triangles[i].vInds[1];
		ind_v2 = mesh->triangles[i].vInds[2];

		a = mesh->vertices[ind_v0];
		b = mesh->vertices[ind_v1];
		c = mesh->vertices[ind_v2];

		u = Subtract(b, a);
		p = Subtract(c, a);

		result = Normalize(CrossProduct(u, p));

		mesh->vnorms[ind_v0] = Add(mesh->vnorms[ind_v0], result);
		mesh->vnorms[ind_v1] = Add(mesh->vnorms[ind_v1], result);
		mesh->vnorms[ind_v2] = Add(mesh->vnorms[ind_v2], result);
	}
	for (int i = 0; i < nv; i++) {
		mesh->vnorms[i] = Normalize(mesh->vnorms[i]);
	}

	mesh->next = *list;
	*list = mesh;
}

// nas insert model za 1.5
//void insertModel(Mesh **list, int nv, float * vArr, int nt, int * tArr, Vector scale, Vector pos, Vector rot, Matrix v, Vector amb, Vector dif, Vector spec, float shin) {
//	Mesh * mesh = (Mesh *) malloc(sizeof(Mesh));
//	mesh->nv = nv;
//	mesh->nt = nt;	
//	mesh->vertices = (Vector *) malloc(nv * sizeof(Vector));
//	mesh->vnorms = (Vector *)malloc(nv * sizeof(Vector));
//	mesh->triangles = (Triangle *) malloc(nt * sizeof(Triangle));
//
//	// Positioning
//	mesh->position = pos;
//	mesh->rotation = rot;
//	mesh->scale = scale;
//
//	// Shading
//	mesh->ambient = amb;
//	mesh->diffuse = dif;
//	mesh->specular = spec;
//	mesh->shininess = shin;
//
//	// set mesh vertices
//	for (int i = 0; i < nv; i++) {
//		mesh->vertices[i].x = vArr[i * 3];
//		mesh->vertices[i].y = vArr[i * 3 + 1];
//		mesh->vertices[i].z = vArr[i * 3 + 2];
//	}
//
//	// set mesh triangles
//	for (int i = 0; i < nt; i++) {
//		mesh->triangles[i].vInds[0] = tArr[i*3];
//		mesh->triangles[i].vInds[1] = tArr[i*3+1];
//		mesh->triangles[i].vInds[2] = tArr[i*3+2];
//	}
//
//	// Assignment 1: 
//	// Calculate and store suitable vertex normals for the mesh here.
//	// Replace the code below that simply sets some arbitrary normal values	
//	int ind_v0, ind_v1, ind_v2;
//	Vector a, b, c;
//	Vector u, p;
//	Vector result;
//	for (int i = 0; i < nt; i++) {  /// <------ nt, not nv
//		ind_v0 = mesh->triangles[i].vInds[0];
//		ind_v1 = mesh->triangles[i].vInds[1];
//		ind_v2 = mesh->triangles[i].vInds[2];
//
//		a = mesh->vertices[ind_v0];
//		b = mesh->vertices[ind_v1];
//		c = mesh->vertices[ind_v2];
//
//		u = Subtract(b, a);
//		p = Subtract(c, a);
//
//		result = Normalize(CrossProduct(u, p));
//		mesh->vnorms[ind_v0] = Add(mesh->vnorms[ind_v0], result);
//		mesh->vnorms[ind_v1] = Add(mesh->vnorms[ind_v1], result);
//		mesh->vnorms[ind_v2] = Add(mesh->vnorms[ind_v2], result);
//	}
//	for (int i = 0; i < nv; i++) {
//		mesh->vnorms[i] = Normalize(mesh->vnorms[i]);
//	}
//
//	mesh->next = *list;
//	*list = mesh;	
//}

*/

void readModel(Mesh** list, const char* name, const char* ext, Vector size, Vector position, Vector rotation) {
	Mesh* mesh = (Mesh*)malloc(sizeof(Mesh));


	/*
		TO DO: ADD PARAMETERS IN FUCNTION FOR THE POSITION AND LIGHT ATTRIBUTES AND ASSIGN HERE
	*/
	mesh->name = std::string(name);
	mesh->ext = std::string(ext);
	mesh->position = position;
	mesh->rotation = rotation;
	mesh->scale = size;

	std::vector<int>vInds_v;
	std::vector<int>tInds_v;
	std::vector<int>nInds_v;

	std::vector<Vector> vertices;
	std::vector<Vector2f> tcord;
	std::vector<Vector> vnorms;

	std::string path;
	path.append(name).append("_model.txt");
	FILE* file = fopen(path.c_str(), "r");
	if (file == NULL) {
		printf("Impossible to open the file !\n");
		printf(path.c_str());
		return;
	}

	while (1) {
		char lineHeader[128];
		// read the first word of the line
		auto res = fscanf(file, "%s", lineHeader);
		if (res == EOF)
			break; // EOF = End Of File. Quit the loop.

		if (strcmp(lineHeader, "v") == 0) {
			Vector v;
			fscanf(file, "%f %f %f\n", &v.x, &v.y, &v.z);
			vertices.push_back(v);
			
		}
		else if (strcmp(lineHeader, "vt") == 0) {
			Vector2f vt;
			fscanf(file, "%f %f\n", &vt.x, &vt.y);
			tcord.push_back(vt);
			
		}
		else if (strcmp(lineHeader, "vn") == 0) {
			Vector norm;
			fscanf(file, "%f %f %f\n", &norm.x, &norm.y, &norm.z);
			vnorms.push_back(norm);
			
		}
		else if (strcmp(lineHeader, "f") == 0){
			int* vInds = new int[4];
			int* tInds = new int[4];
			int* nInds = new int[4];

			int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d %d/%d/%d\n", 
				&vInds[0], &tInds[0], &nInds[0],
				&vInds[1], &tInds[1], &nInds[1],
				&vInds[2], &tInds[2], &nInds[2],
				&vInds[3], &tInds[3], &nInds[3]  );



			if (matches == 12) {
				for (int i = 0; i < 4; i++) {
					nInds[i]--;
					tInds[i]--;
					vInds[i]--;
				}

				// 1st triangle
				nInds_v.push_back(nInds[0]);
				tInds_v.push_back(tInds[0]);
				vInds_v.push_back(vInds[0]);
					 
				nInds_v.push_back(nInds[1]);
				tInds_v.push_back(tInds[1]);
				vInds_v.push_back(vInds[1]);
					 
				nInds_v.push_back(nInds[2]);
				tInds_v.push_back(tInds[2]);
				vInds_v.push_back(vInds[2]);

				// 2nd triangle

				nInds_v.push_back(nInds[0]);
				tInds_v.push_back(tInds[0]);
				vInds_v.push_back(vInds[0]);

				nInds_v.push_back(nInds[2]);
				tInds_v.push_back(tInds[2]);
				vInds_v.push_back(vInds[2]);
					 
				nInds_v.push_back(nInds[3]);
				tInds_v.push_back(tInds[3]);
				vInds_v.push_back(vInds[3]);
			}
			else {
				matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n",
					&vInds[0], &tInds[0], &nInds[0],
					&vInds[1], &tInds[1], &nInds[1],
					&vInds[2], &tInds[2], &nInds[2]);

				for (int i = 0; i < 3; i++) {
					nInds[i]--;
					tInds[i]--;
					vInds[i]--;

					nInds_v.push_back(nInds[i]);
					tInds_v.push_back(tInds[i]);
					vInds_v.push_back(vInds[i]);
				}
			}
			
			
		}
	}
	
	mesh->vertexData.resize(vInds_v.size());

	for (int i = 0; i < vInds_v.size(); i++) {
		mesh->vertexData[i].position = vertices[vInds_v[i]];
		mesh->vertexData[i].normal = vnorms[nInds_v[i]];
		mesh->vertexData[i].texcoord = tcord[tInds_v[i]];
	}

	mesh->next = *list;
	*list = mesh;	
}



void refreshTMat(Camera& cam, Matrix& t) {
	t.e[12] = -cam.position.x;
	t.e[13] = -cam.position.y;
	t.e[14] = -cam.position.z;
}

void refreshRotMats(Camera& cam, Matrix& rx, Matrix& ry, Matrix& rz) {
	float alpha = toRadian(-cam.rotation.x);
	float beta = toRadian(-cam.rotation.y);
	float gama = toRadian(-cam.rotation.z);
	rx.e[5] = cos(alpha);
	rx.e[6] = sin(alpha);
	rx.e[9] = -rx.e[6];
	rx.e[10] = rx.e[5];

	ry.e[0] = cos(beta);
	ry.e[2] = -sin(beta);
	ry.e[8] = -ry.e[2];
	ry.e[10] = ry.e[0];

	rz.e[0] = cos(gama);
	rz.e[1] = sin(gama);
	rz.e[4] = -rz.e[1];
	rz.e[5] = rz.e[0];
}

Matrix compTransf(Vector& vt, Vector& vr, Vector& vs) {
	Matrix t = translationMat(vt);
	Matrix r = rotMat(vr);
	Matrix s = scaleMat(vs);

	return MatMatMul(t, MatMatMul(r, s));
}

Matrix translationMat(Vector& v) {
	Matrix t;
	unitMat(t);

	t.e[12] = v.x;
	t.e[13] = v.y;
	t.e[14] = v.z;

	return t;
}

Matrix rotMat(Vector& v) {
	Matrix rx, ry, rz;
	rotXMat(rx, v.x);
	rotYMat(ry, v.y);
	rotZMat(rz, v.z);

	return MatMatMul(rx, MatMatMul(ry, rz));
}

void rotXMat(Matrix& m, float x) {
	unitMat(m);
	x = toRadian(x);
	m.e[5] = cos(x);
	m.e[6] = sin(x);
	m.e[9] = -m.e[6];
	m.e[10] = m.e[5];
}

void rotYMat(Matrix& m, float x) {
	unitMat(m);
	x = toRadian(x);
	m.e[0] = cos(x);
	m.e[2] = -sin(x);
	m.e[8] = -m.e[2];
	m.e[10] = m.e[0];
}

void rotZMat(Matrix& m, float x) {
	unitMat(m);
	x = toRadian(x);
	m.e[0] = cos(x);
	m.e[1] = sin(x);
	m.e[4] = -m.e[1];
	m.e[5] = m.e[0];
}

Matrix scaleMat(Vector& v) {
	Matrix m;
	unitMat(m);
	m.e[0] = v.x;
	m.e[5] = v.y;
	m.e[10] = v.z;

	return m;
}
