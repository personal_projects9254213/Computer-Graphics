#ifndef _MESH_H_
#define _MESH_H_

#include "algebra.h"
#include <string>
#include<vector>

typedef struct _Triangle {    // NOT IN USE!!!!!!!!!!!!!!!!!!!
	int vInds[3]; //vertex indices
	int tInds[3]; //texture indices
	int nInds[3]; //normal indices
} Triangle;


typedef struct _Vertex {    /*  x y z       u v        xyz*/
	Vector position;
	Vector2f texcoord;
	Vector normal;
}Vertex;

typedef struct _Mesh { 
	
	std::string name;
	std::vector<Vertex> vertexData;
	std::string ext;
	//position
	Vector position;
	Vector rotation;
	Vector scale;
	//lighting
	//Vector ambient;
	//Vector diffuse;
	//Vector specular;
	//double shininess;

	struct _Mesh *next; 
	unsigned int vbo, ibo, vao; // OpenGL handles for rendering
} Mesh;

typedef struct _Light {
	Vector position;
	Vector ambient;
	Vector diffuse;
	Vector specular;
}Light;

typedef struct _Camera {
	Vector position;
	Vector rotation;
	double fov; 
	double nearPlane; 
	double farPlane; 
} Camera;

void readModel(Mesh** list, const char* name, const char* ext, Vector size, Vector position, Vector rotation);
void insertModel(Mesh** objlist, int nv, float* vArr, int nt, int* tArr, float scale);
void insertModel(Mesh ** objlist, int nv, float * vArr, int nt, int * tArr, Vector scale, Vector pos, Vector rot, Matrix v, Vector amb, Vector dif, Vector spec, float shin);
void refreshTMat(Camera& cam, Matrix& t);
void refreshRotMats(Camera& cam, Matrix& rx, Matrix& ry, Matrix& rz);
Matrix compTransf(Vector& vt, Vector& vr, Vector& vs);
Matrix translationMat(Vector& v);
Matrix rotMat(Vector& v);
void rotXMat(Matrix& m, float x);
void rotYMat(Matrix& m, float x);
void rotZMat(Matrix& m, float x);
Matrix scaleMat(Vector& v);
float toRadian(float x);
void transformMesh(Mesh* mesh, Matrix v, bool selected);
#endif
