#define _CRT_SECURE_NO_WARNINGS_
#pragma warning(disable : 4996)
#include <fstream>

// Simple vertex shader treating vertex normals as RGB colors
//static const char * vs_n2c_src[] = {
//	"#version 420 core                                                 \n"
//	"                                                                  \n"
//	" in vec3 vPos;                                                    \n"
//	" in vec3 vNorm;                                                   \n"
//	" out vec4 color;                                                  \n"
//	" uniform mat4 PV;                                                 \n"
//	"                                                                  \n"
//	"void main(void)                                                   \n"
//	"{                                                                 \n"
//	"    color = abs(vec4(vNorm, 1.0));                                \n"
//	"    gl_Position = PV * vec4(vPos, 1.0f);                          \n"
//	"}                                                                 \n"
//};

// Simple fragment shader for color interpolation
//static const char * fs_ci_src[] = {
//	"#version 420 core                                                 \n"
//	"                                                                  \n"
//	"in vec4 color;                                                    \n"
//	"out vec4 fColor;                                                  \n"
//	"                                                                  \n"
//	"void main(void)                                                   \n"
//	"{                                                                 \n"
//	"    fColor = color;                                               \n"
//	"}                                                                 \n"
//};



char** readShdrFile(const char* path) {
	char buf[256];
	int line_cnt = 0;
	char** shader;
	std::ifstream file;

	//get number of lines
	file.open(path);
	while (file.getline(buf, 256)) line_cnt++;
	file.close();

	shader = new char* [1];
	shader[0] = new char[line_cnt * 256];
	shader[0][0] = '\0';

	file.open(path);
	while (file.getline(buf, 256)) {
		strcat(shader[0], buf);
		strcat(shader[0], "\0");
		strcat(shader[0], "\n");
		strcat(shader[0], "\0");
	}
	file.close();
	return shader;
}

// The source code for additional shaders can be added here
// ...
