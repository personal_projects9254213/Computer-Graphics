//#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <glew.h>
#include <freeglut.h>
#include <iostream>

#include "algebra.h"
#include "shaders.h"
#include "mesh.h"
#include<vector>
#include<string>
#include <fstream>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
// Include data for some triangle meshes (hard coded in struct variables)

int screen_width = 1024;
int screen_height = 768;

float scale = 2.0f;

Mesh *meshList = NULL; // Global pointer to linked list of triangle meshes

Mesh* selected_mesh;

Camera cam = {{0.0,0.0,20.0}, {0.0,0.0,180.0}, 60.0, 1.0f, 10000.0f}; // Setup the global camera parameters
//Camera cam = { {0.0,0.0,20.0}, {0.0,0.0,0.0}, 60.0, 1, 10000 };


GLuint shprg[3]; // Shader program id

float select_mode = 1.0f;

Light lights[4];

std::vector<std::string> faces
{
		"models/right.jpg",
		"models/left.jpg",
		"models/bottom.jpg",
		"models/top.jpg",
		"models/front.jpg",
		"models/back.jpg"
};

unsigned int skyboxVAO, skyboxVBO, cubemapTexture;
int prog_count=1;

const float _LEFT = -20.0f;
const float _RIGHT = 20.0f;
const float _BOT = -10.0f;
const float _TOP = 10.0f;
const float _NEAR = cam.nearPlane;
const float _FAR = cam.farPlane;

float aspect = (float)screen_width / (float)screen_height;

// Global transform matrices
Matrix V, P, PV; // View and Perspective
Matrix R, Rx, Ry, Rz; // Rotations
Matrix T; // Translation
Matrix S; // Scaling 

unsigned int* texture;

//char** vs_src = readShdrFile("phong_vert_shdr.glsl");
//char** gs_src = readShdrFile("phong_geom_shdr.glsl");
//char** fs_src = readShdrFile("phong_frag_shdr.glsl");

const char* src[] = { "pbr_vs.glsl", nullptr ,"pbr_fs.glsl",
					"skybox_vs.glsl",nullptr,"skybox_fs.glsl"
};

char*** shd_src = new char**[6];


void prepareShaderProgram(char** vs_src, char** gs_src, char** fs_src, int prog_id) {
	GLint success = GL_FALSE;

	shprg[prog_id] = glCreateProgram();

	GLuint vs = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs, 1, vs_src, NULL);
	glCompileShader(vs);
	glGetShaderiv(vs, GL_COMPILE_STATUS, &success);	
	if (!success) {
		GLint maxLength = 0;
		glGetShaderiv(vs, GL_INFO_LOG_LENGTH, &maxLength);

		// The maxLength includes the NULL character
		std::vector<GLchar> errorLog(maxLength);
		glGetShaderInfoLog(vs, maxLength, &maxLength, &errorLog[0]);
		std::string s(errorLog.begin(), errorLog.end());

		printf("Error in vertex shader!\n");
		fflush(stdout);
		printf("%s\n", s);
	}
	else printf("Vertex shader compiled successfully!\n");

	
	
	GLuint gs = glCreateShader(GL_GEOMETRY_SHADER);
	if (gs_src != nullptr) {
		glShaderSource(gs, 1, gs_src, NULL);
		glCompileShader(gs);
		glGetShaderiv(gs, GL_COMPILE_STATUS, &success);
		if (!success) {
			GLint maxLength = 0;
			glGetShaderiv(gs, GL_INFO_LOG_LENGTH, &maxLength);

			// The maxLength includes the NULL character
			std::vector<GLchar> errorLog(maxLength);
			glGetShaderInfoLog(gs, maxLength, &maxLength, &errorLog[0]);
			std::string s(errorLog.begin(), errorLog.end());

			printf("Error in geometry shader!\n");
			printf("%s\n", s);
		}
		else printf("Geometry shader compiled successfully!\n");
	}


	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs, 1, fs_src, NULL);
	glCompileShader(fs);
	glGetShaderiv(fs, GL_COMPILE_STATUS, &success);	
	if (!success) {
		GLint maxLength = 0;
		glGetShaderiv(fs, GL_INFO_LOG_LENGTH, &maxLength);

		// The maxLength includes the NULL character
		std::vector<GLchar> errorLog(maxLength);
		glGetShaderInfoLog(fs, maxLength, &maxLength, &errorLog[0]);
		std::string s(errorLog.begin(), errorLog.end());

		printf("Error in fragment shader!\n");
		printf("%s\n", s);
	}
	else {
		printf("Fragment shader compiled successfully!\n");
	}

	glAttachShader(shprg[prog_id], vs);
	if (gs_src != nullptr )glAttachShader(shprg[prog_id], gs);
	glAttachShader(shprg[prog_id], fs);
	glLinkProgram(shprg[prog_id]);

	GLint isLinked = GL_FALSE;
	glGetProgramiv(shprg[prog_id], GL_LINK_STATUS, &isLinked);
	if (!isLinked) {
		printf("Link error in linker program!\n");
		GLint maxLength = 0;
		glGetShaderiv(shprg[prog_id], GL_INFO_LOG_LENGTH, &maxLength);

		// The maxLength includes the NULL character
		std::vector<GLchar> errorLog(maxLength);
		glGetProgramInfoLog(shprg[prog_id], maxLength, &maxLength, &errorLog[0]);
		std::string s(errorLog.begin(), errorLog.end());
		glDeleteProgram(shprg[prog_id]);
		printf("Error in linker!\n");
		printf("%s\n", s);
	} 
	else printf("Shader program linked successfully!\n");
}

unsigned int loadCubemap(std::vector<std::string> faces) {
	unsigned int skyboxTextureId;
	glGenTextures(1, &skyboxTextureId);
	glBindTexture(GL_TEXTURE_CUBE_MAP, skyboxTextureId);

	int width, height, nrChannels;
	for (unsigned int i = 0; i < faces.size(); i++)
	{
		unsigned char* data = stbi_load(faces[i].c_str(), &width, &height, &nrChannels, 0);
		if (data)
		{
			
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
				0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data
			);
			stbi_image_free(data);
		}
		else
		{
			std::cout << "Cubemap tex failed to load at path: " << faces[i] << std::endl;
			stbi_image_free(data);
		}
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	return skyboxTextureId;
}

unsigned int loadTexture(char const* path)
{
	unsigned int textureID;
	glGenTextures(1, &textureID);

	int width, height, nrComponents;
	unsigned char* data = stbi_load(path, &width, &height, &nrComponents, 0);
	if (data)
	{
		GLenum format;
		if (nrComponents == 1)
			format = GL_RED;
		else if (nrComponents == 3)
			format = GL_RGB;
		else if (nrComponents == 4)
			format = GL_RGBA;

		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		stbi_image_free(data);
	}
	else
	{
		std::cout << "Texture failed to load at path: " << path << std::endl;
		stbi_image_free(data);
	}

	return textureID;
}

void prepareMesh(Mesh *mesh, int prog_id) {
	int vertDataSize = mesh->vertexData.size() * sizeof(Vertex);
	
	// For storage of state and other buffer objects needed for vertex specification
	glGenVertexArrays(1, &mesh->vao);
	glBindVertexArray(mesh->vao);

	// Allocate VBO and load mesh data (vertices and normals)
	glGenBuffers(1, &mesh->vbo);
	glBindBuffer(GL_ARRAY_BUFFER, mesh->vbo);
	glBufferData(GL_ARRAY_BUFFER, vertDataSize, mesh->vertexData.data(), GL_STATIC_DRAW);

	// Allocate index buffer and load mesh indices
	glGenBuffers(1, &mesh->ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->ibo);

	std::vector<int> indicies;
	indicies.resize(mesh->vertexData.size());
	for (int i = 0; i < indicies.size(); i++) indicies[i] = i;
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicies.size() * sizeof(int) , indicies.data(), GL_STATIC_DRAW);
	//-----------------
	//// Define the format of the vertex data
	GLint vPos = glGetAttribLocation(shprg[prog_id], "vPos");
	glVertexAttribPointer(vPos, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, position));
	glEnableVertexAttribArray(vPos);
	// Define the format of the texture data 
	GLint vTex = glGetAttribLocation(shprg[prog_id], "vTex");
	glVertexAttribPointer(vTex, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, texcoord));
	glEnableVertexAttribArray(vTex);
	// Define the format of the normal data
	GLint vNorm = glGetAttribLocation(shprg[prog_id], "vNorm");
	glVertexAttribPointer(vNorm, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, normal));
	glEnableVertexAttribArray(vNorm);

	glBindVertexArray(0);
	
	//*************** TEXTURE LOADING ************************
	texture = new unsigned int[5];
	glGenTextures(5, texture);
	glActiveTexture(texture[0]);
	glBindTexture(GL_TEXTURE_2D, texture[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER); //gl repeat
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); //GL_LINEAR_MIPMAP_LINEAR
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	int width, height, nrChannels;
	// Loading albedo texture
	auto path = std::string("models/").append(std::string(mesh->name)).append("_albedo.").append(mesh->ext);
	stbi_set_flip_vertically_on_load(1);
	unsigned char* data = stbi_load(path.c_str(), &width, &height, &nrChannels, STBI_rgb);
	if (data) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else {
		std::cout << path << std::endl;
		std::cout << "texture could not be loaded!" << std::endl;
	}

	glActiveTexture(0);
	glBindTexture(GL_TEXTURE_2D, 0);
	stbi_image_free(data);
	//--------------------------------------

	// Loading metallic texture
	glBindTexture(GL_TEXTURE_2D, texture[1]);
	glActiveTexture(texture[1]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	path = std::string("models/").append(std::string(mesh->name)).append("_metallic.").append(mesh->ext);
	data = stbi_load(path.c_str(), &width, &height, &nrChannels, 0);
	if (data) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, width, height, 0, GL_RED, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);		
	}
	else {
		std::cout << path << std::endl;
		std::cout << "texture could not be loaded!" << std::endl;
	}
	glActiveTexture(0);
	glBindTexture(GL_TEXTURE_2D, 0);
	stbi_image_free(data);
	//--------------------------------------
	//Loading roughness texture
	glBindTexture(GL_TEXTURE_2D, texture[2]);
	path = std::string("models/").append(std::string(mesh->name)).append("_rough.").append(mesh->ext);
	data = stbi_load(path.c_str(), &width, &height, &nrChannels, 0);
	if (data) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, width, height, 0, GL_RED, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
		
	}
	else {
		std::cout << path << std::endl;
		std::cout << "texture could not be loaded!" << std::endl;
	}
	glActiveTexture(0);
	glBindTexture(GL_TEXTURE_2D, 0);
	stbi_image_free(data);
	//--------------------------------------
	// Loading normal texture
	glBindTexture(GL_TEXTURE_2D, texture[3]);
	path = std::string("models/").append(std::string(mesh->name)).append("_normal.").append(mesh->ext);
	data = stbi_load(path.c_str(), &width, &height, &nrChannels, 0);
	if (data) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
		
	}
	else {
		std::cout << path << std::endl;
		std::cout << "texture could not be loaded!" << std::endl;
	}
	glActiveTexture(0);
	glBindTexture(GL_TEXTURE_2D, 0);
	stbi_image_free(data);
	//--------------------------------------
	// Loading ambient oclussion  texture 
	glBindTexture(GL_TEXTURE_2D, texture[4]);
	path = std::string("models/").append(std::string(mesh->name)).append("_ao.").append(mesh->ext);
	data = stbi_load(path.c_str(), &width, &height, &nrChannels, STBI_rgb);
	if (data) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
		
	}
	else {
		std::cout << path << std::endl;
		std::cout << "texture could not be loaded!" << std::endl;
	}
	glActiveTexture(0);
	glBindTexture(GL_TEXTURE_2D, 0);
	stbi_image_free(data);
	//--------------------------------------

}


void prepareSkybox() {
	cubemapTexture = loadCubemap(faces);
	float skyboxVertices[] = {
		// positions          
		-1.0f,  1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		 1.0f, -1.0f, -1.0f,
		 1.0f, -1.0f, -1.0f,
		 1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		 1.0f, -1.0f, -1.0f,
		 1.0f, -1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f, -1.0f,
		 1.0f, -1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		 1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		-1.0f,  1.0f, -1.0f,
		 1.0f,  1.0f, -1.0f,
		 1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		 1.0f, -1.0f, -1.0f,
		 1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		 1.0f, -1.0f,  1.0f
	};
	
	// skybox VAO
	
	glGenVertexArrays(1, &skyboxVAO);
	glGenBuffers(1, &skyboxVBO);
	glBindVertexArray(skyboxVAO);
	glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
}

void renderSkybox(int prog_id) {
	// draw skybox as last
	glDepthFunc(GL_LEQUAL);  // change depth function so depth test passes when values are equal to depth buffer's content
	
	GLint loc_P = glGetUniformLocation(shprg[prog_id], "position");
	glUniformMatrix4fv(loc_P, 1, GL_FALSE, P.e);

	Matrix _V;
	for (int i = 0; i < 12; i++)
		_V.e[i] = V.e[i];
	_V.e[12] = _V.e[13] = _V.e[14] = 0.0f; _V.e[15] = 1.0f;
	_V.e[3] = _V.e[7] = _V.e[11] = 0.0f;

	GLint loc_V = glGetUniformLocation(shprg[prog_id], "view");
	glUniformMatrix4fv(loc_V, 1, GL_FALSE, _V.e);
	// skybox cube
	glBindVertexArray(skyboxVAO);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapTexture);
	glUniform1i(glGetUniformLocation(shprg[prog_id], "skybox"), 0);

	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
	glDepthFunc(GL_LESS); // set depth function back to default
}

void renderMesh(Mesh *mesh,int prog_id) {
	// Assignment 1: Apply the transforms from local mesh coordinates to world coordinates here
	// Combine it with the viewing transform that is passed to the shader below

	//Za cvor zakomentarisati sve ispod W M PM
	Matrix W = compTransf(mesh->position, mesh->rotation, mesh->scale);   
	Matrix M = MatMatMul(V, W);
	Matrix PM = MatMatMul(P, M);
	//Ovde stati sa komentarisanjem

	// Pass the viewing transform to the shader

	GLint loc_P = glGetUniformLocation(shprg[prog_id], "P");
	glUniformMatrix4fv(loc_P, 1, GL_FALSE, P.e);

	GLint loc_V = glGetUniformLocation(shprg[prog_id], "V");
	glUniformMatrix4fv(loc_V, 1, GL_FALSE, V.e);

	GLint loc_PM = glGetUniformLocation(shprg[prog_id], "PM");
	glUniformMatrix4fv(loc_PM, 1, GL_FALSE, PM.e);  // <--- Za Cvor ide PV.e , za zivotinjke PM.e

	GLint loc_M = glGetUniformLocation(shprg[prog_id], "M");
	glUniformMatrix4fv(loc_M, 1, GL_FALSE, M.e);

	GLint loc_W = glGetUniformLocation(shprg[prog_id], "W");
	glUniformMatrix4fv(loc_W, 1, GL_FALSE, W.e);
	// Select current resources 
	glBindVertexArray(mesh->vao);

	//===============Textures========================

	glActiveTexture(GL_TEXTURE6);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapTexture);
	glUniform1i(glGetUniformLocation(shprg[prog_id], "skybox"), 6);

	//==================================================================

	// Bind texture

	glUniform1i(glGetUniformLocation(shprg[prog_id], "AlbedoMap"), 0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture[0]);

	glUniform1i(glGetUniformLocation(shprg[prog_id], "MetallicMap"), 1);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, texture[1]);

	glUniform1i(glGetUniformLocation(shprg[prog_id], "RoughMap"), 2);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, texture[2]);

	glUniform1i(glGetUniformLocation(shprg[prog_id], "NormalMap"), 3);
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, texture[3]);

	glUniform1i(glGetUniformLocation(shprg[prog_id], "AoMap"), 4);
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, texture[4]);


	/// ===============================================




	// Camera
	GLint loc_camPos = glGetUniformLocation(shprg[prog_id], "camPos");
	glUniform3f(loc_camPos, cam.position.x, cam.position.y, cam.position.z);

	GLint loc_mode = glGetUniformLocation(shprg[prog_id], "mode");
	glUniform1f(loc_mode, select_mode);

	// Draw all triangles
	glDrawElements(GL_TRIANGLES, mesh->vertexData.size(), GL_UNSIGNED_INT, NULL); 
	glBindVertexArray(shprg[prog_id]);
}

void initMatrices() {
	// Initialize View matrix

	//Initialize P matrix

	//matOrtho(P, scale, _LEFT, _RIGHT, _BOT, _TOP, _NEAR, _FAR);
	matPersp(P, cam.fov, aspect, _NEAR, _FAR);
	unitMat(V);
	refreshTMat(cam,V);
	unitMat(R);
	unitMat(T);
	unitMat(Rx);
	unitMat(Ry);
	unitMat(Rz);
	unitMat(S);
}

void display(void) {
	Mesh *mesh;

	// 1.3
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //
	glDepthMask(GL_TRUE);
	// Assignment 1: Calculate the transform to view coordinates yourself 	
	// The matrix V should be calculated from camera parameters
	// Therefore, you need to replace this hard-coded transform. 
	refreshTMat(cam, T);
	refreshRotMats(cam, Rx, Ry, Rz);
	R = MatMatMul(Rz, MatMatMul(Ry, Rx));
	V = MatMatMul(R, T);
	// Assignment 1: Calculate the projection transform yourself 	
	// The matrix P should be calculated from camera parameters
	// Therefore, you need to replace this hard-coded transform. 	

	// This finds the combined view-projection matrix
	PV = MatMatMul(P, V);

	// Select the shader program to be used during rendering 

	glUseProgram(shprg[0]);
	// Prosledi svetlo za sencenje
	for (int i = 0; i < 4; i++) {

		GLint loc_lightPos = glGetUniformLocation(shprg[0], ("light["+ std::to_string(i) +"].Position").c_str());
		glUniform3f(loc_lightPos, lights[i].position.x, lights[i].position.y, lights[i].position.z);

		GLint loc_lightLs = glGetUniformLocation(shprg[0], ("light[" + std::to_string(i) + "].Ls").c_str());
		glUniform3f(loc_lightLs, lights[i].specular.x, lights[i].specular.y, lights[i].specular.z);
	}
	

	// Render all meshes in the scene
	mesh = meshList;
	while (mesh != NULL) {
		renderMesh(mesh, 0);
		mesh = mesh->next;
	}

	glUseProgram(shprg[1]);
	renderSkybox(1);

	
	glFlush();
}

void changeSize(int w, int h) {
	screen_width = w;
	screen_height = h;
	glViewport(0, 0, screen_width, screen_height);
}

void keypress(unsigned char key, int x, int y) {
	float camera_step = 0.5f;
	float rot_step = 1.0f;
	float model_step = 0.25f;
	float model_rot_step = 1.0f;
	float model_size_step = 1.0f;
	float light_step = 0.2f;
	GLint tmp = shprg[0];
	switch(key) {
	// CAMERA SETTINGS START
	case 'x':
		cam.position.x -= camera_step;
		break;
	case 'X':
		cam.position.x += camera_step;
		break;
	case 'y':
		cam.position.y -= camera_step;
		break;
	case 'Y':
		cam.position.y += camera_step;
		break;
	case 'z':
		cam.position.z -= camera_step;
		break;
	case 'Z':
		cam.position.z += camera_step;
		break;
	case 'i':
		cam.rotation.x -= rot_step;
		break;
	case 'I':
		cam.rotation.x += rot_step;
		break;
	case 'j':
		cam.rotation.y -= rot_step;
		break;
	case 'J':
		cam.rotation.y += rot_step;
		break;
	case 'k':
		cam.rotation.z -= rot_step;
		break;
	case 'K':
		cam.rotation.z += rot_step;
		break;
	
	// CAMERA SETTINGS END

	case 'm':
		shprg[0] = shprg[2];
		shprg[2] = tmp;
		break;

	// MODEL SETTINGS START
	case 'a':
		selected_mesh = selected_mesh->next;
		if (selected_mesh == nullptr) {
			selected_mesh = meshList;
		}
		break;
	case 's':
		selected_mesh->rotation.x += model_rot_step;
		break;
	case 'S':
		selected_mesh->rotation.x -= model_rot_step;
		break;
	case 'd':
		selected_mesh->rotation.y += model_rot_step;
		break;
	case 'D':
		selected_mesh->rotation.y -= model_rot_step;
		break;
	case 'f':
		selected_mesh->rotation.z += model_rot_step;
		break;
	case 'F':
		selected_mesh->rotation.z -= model_rot_step;
		break;
	case 'w':
		selected_mesh->position.x += model_step;
		break;
	case 'W':
		selected_mesh->position.x -= model_step;
		break;
	case 'e':
		selected_mesh->position.y += model_step;
		break;
	case 'E':
		selected_mesh->position.y -= model_step;
		break;
	case 'r':
		selected_mesh->position.z += model_step;
		break;
	case 'R':
		selected_mesh->position.z -= model_step;
		break;
	case 't':
		selected_mesh->scale.x += model_size_step;
		selected_mesh->scale.y += model_size_step;
		selected_mesh->scale.z += model_size_step;
		break;
	case 'T':
		selected_mesh->scale.x -= model_size_step;
		selected_mesh->scale.y -= model_size_step;
		selected_mesh->scale.z -= model_size_step;
		break;
	/*case 'l':
		if (light.specular.x + 0.01f <= 1.0f) {
			light.specular.x += 0.01f;
			light.specular.y += 0.01f;
			light.specular.z += 0.01f;
		}		
		break;
	case 'L':
		if (light.specular.x - 0.01f >= 0.0f) {
			light.specular.x -= 0.01f;
			light.specular.y -= 0.01f;
			light.specular.z -= 0.01f;
		}
		break;
	case 'c':
		light.position.x += light_step;			
		break;	
	case 'C':
		light.position.x -= light_step;		
		break;
	case 'v':
		light.position.y += light_step;
		break;
	case 'V':
		light.position.y -= light_step;
		break;
	case 'b':
		light.position.z += light_step;
		break;
	case 'B':
		light.position.z -= light_step;
		break;*/
	// MODEL SETTINGS END
	case '1':
		select_mode = 1.0f;
		break;
	case '2':
		select_mode = 2.0f;
		break;
	case '3':
		select_mode = 3.0f;
		break;
	case '4':
		select_mode = 4.0f;
		break;
	case '5':
		select_mode = 5.0f;
		break;
	case '6':
		select_mode = 6.0f;
		break;
	case 'Q':
	case 'q':		
		glutLeaveMainLoop();
		break;
	default:
		return;
	}
	glutPostRedisplay();
}

void init(void) {
	// Compile and link the given shader program (vertex shader and fragment shader)

	for (int i = 0; i < 6; i++) {
		if (src[i] != nullptr)
			shd_src[i] = readShdrFile(src[i]);
		else
			shd_src[i] = nullptr;
	}
	
	for (int i = 0; i < 2; i++) {
		prepareShaderProgram(shd_src[3*i], shd_src[3 * i +1], shd_src[3 * i+2], i);
	}
		// Setup OpenGL buffers for rendering of the meshes
		Mesh* mesh = meshList;
		while (mesh != NULL) {
			prepareMesh(mesh,0);
			mesh = mesh->next;
		}

		prepareSkybox();
	
}

void cleanUp(void) {	
	printf("Running cleanUp function... ");
	// Free openGL resources
	// ...

	// Free meshes
	// ...
	printf("Done!\n\n");
}

int main(int argc, char **argv) {
	
	lights[0] = { {1.0,1.0,1.0},{0.2,0.2,0.2}, {1.0,1.0,1.0},{1.0,1.0,1.0} };
	lights[1] = { {-1.0,1.0,1.0},{0.2,0.2,0.2}, {1.0,1.0,1.0},{1.0,1.0,1.0} };
	lights[2] = { {1.0,-1.0,1.0},{0.2,0.2,0.2}, {1.0,1.0,1.0},{1.0,1.0,1.0} };
	lights[3] = { {-1.0,-1.0,1.0},{0.2,0.2,0.2}, {1.0,1.0,1.0},{1.0,1.0,1.0} };


	// Setup freeGLUT
	initMatrices();
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	// glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(screen_width, screen_height);
	glutCreateWindow("DVA338 Programming Assignments");
	glutDisplayFunc(display);
	glutReshapeFunc(changeSize);
	glutKeyboardFunc(keypress);
	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

	// Specify your preferred OpenGL version and profile
	glutInitContextVersion(4, 5);
	//glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);	
	glutInitContextProfile(GLUT_CORE_PROFILE);

	// Uses GLEW as OpenGL Loading Library to get access to modern core features as well as extensions
	GLenum err = glewInit(); 
	if (GLEW_OK != err) { fprintf(stdout, "Error: %s\n", glewGetErrorString(err)); return 1; }

	// Output OpenGL version info
	fprintf(stdout, "GLEW version: %s\n", glewGetString(GLEW_VERSION));
	fprintf(stdout, "OpenGL version: %s\n", (const char *)glGetString(GL_VERSION));
	fprintf(stdout, "OpenGL vendor: %s\n\n", glGetString(GL_VENDOR));

	readModel(&meshList, "dagger","png", { 1.0,1.0,1.0 }, { 0.00,-2.0,0.0 }, { 0.0,0.0,180.0 });
	//readModel(&meshList, "sword","png", { 0.08,0.08,0.08 }, { 0.00,0.0,0.0 }, { 0.0,0.0,0.0 });
	//readModel(&meshList, "base", "jpg", {1.0,1.0,1.0 }, { 0.0,-1.4,-2.0 }, { 0.0,90.0,0.0 });

	selected_mesh = meshList;

	init();
	glutMainLoop();

	cleanUp();	
	return 0;
}
