#include "Sphere.h"


bool Sphere::hit(const Ray& r, HitRec& rec, int index) const {

	//Vec3f v = c - r.o;
	//float s = v.dot(r.d);
	//float vLenSq = v.dot(v);
	//float radSq = this->r * this->r;
	//if (s < 0 && vLenSq > radSq) {
	//	return false;
	//}
	//float mSq = vLenSq - s * s;
	//if (mSq > radSq) {
	//	return false;
	//}
	Vec3f O_C = (r.o - this->c); // O - C
	float A = r.d.dot(r.d);
	float B = 2 * (O_C.dot(r.d));
	float C = O_C.dot(O_C) - this->r * this->r;
	float D = B * B - 4 * C * A;
	if (D < 0)
		return false;
	float t1 = (-B + (float)sqrt(D)) / (2.0f*A);
	float t2 = (-B - (float)sqrt(D)) / (2.0f * A);

	if (t1 < 0 && t2 < 0)
		return false;

	float result;

	if (t1 < 0)
		result = t2;
	else if (t2 < 0)
		result = t1;
	else
		result = fmin(t1, t2);
	if (result < rec.tHit) {
		rec.tHit = result;
		rec.anyHit = true;
		rec.primIndex = index;
	}

	return true;
}


void Sphere::computeSurfaceHitFields(const Ray& r, HitRec& rec) const {
	rec.p = r.o + r.d * rec.tHit;
	rec.n = (rec.p - c).normalize();
}
