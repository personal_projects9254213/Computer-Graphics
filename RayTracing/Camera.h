#ifndef _CAMERA_H_
#define _CAMERA_H_
#include "Vec3.h"
class Camera {
private:
	Vec3f pos;
	Vec3f rot;
public:
	Camera(const Vec3f& position, const Vec3f& rotation) {
		this->pos = position;
		this->rot = rotation;
	}

	Vec3f getPos() {
		return this->pos;
	}

	void adjustZPos(float val) {
		this->pos.z += val;
	}
	void adjustXPos(float val) {
		this->pos.x += val;
	}
	void adjustYPos(float val) {
		this->pos.y += val;
	}
};
#endif