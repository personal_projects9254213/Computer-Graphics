#ifndef _LIGHT_H_
#define _LIGHT_H_

#include "Vec3.h"

class Light {
public:
	Vec3f position;
	Vec3f Id;
	Vec3f Ia;
	Vec3f Is;

	Light(const Vec3f& pos, const Vec3f& ia, const Vec3f& id, const Vec3f& is) : position(pos), Ia(ia), Id(id), Is(is) {}
};

#endif