#include<vector>
#include<iostream>
#include <chrono>
using namespace std;

#include <glut.h>

#include "Vec3.h"
#include "Image.h"
#include "Ray.h"
#include "Sphere.h"
#include "Camera.h"
#include "Light.h"
#include <omp.h>

Camera* cam = new Camera(Vec3f(0.0f, 0.0f, 10.0f), Vec3f(0.0f, 0.0f, 0.0f));
int interection_count = 0;
int nthreads = 4;
bool flag = true;
float toRadian(float x) { return x * 3.1415927 / 180.0; }

Vec3f getReflectionVector(Vec3f& light, Vec3f& norm) {
	float L_N = light.dot(norm);
	return ( ( norm * L_N) * 2  - light);
}

class Scene {
public:
	vector<Sphere> spheres;
	vector<Light> lights;

	Scene(void) {

	}
	void add(Sphere& s) {
		spheres.push_back(s);
		//cout << "Sphere added: " << "r = " << spheres[spheres.size()-1].r << endl;
	}

	void add(Light& lght) {
		lights.push_back(lght);
	}



	void load(char* fileName) {
		// load a file with spheres for your scene here ...
		// Note: You do not have to do this as part of the assignment.
		// This is for the sake of convenience, if you want to save and setup many interesting scenes
	}

};


void glSetPixel(int& x, int& y, Vec3f& c) {
	glColor3f(c.r, c.g, c.b);
	glBegin(GL_POINTS);
	glVertex2i(x, y);
	glEnd();
}

class SimpleRayTracer {
private:
	Scene* scene;
	Image* image;
	Camera* camera;

	Vec3f getEyeRayDirection(int x, int y) {
		//Uses a fix camera looking along the negative z-axis
		static float z = -5.0f;
		static float sizeX = 4.0f;
		static float sizeY = 3.0f;
		static float left = -sizeX * 0.5f;
		static float bottom = -sizeY * 0.5f;
		static float dx = sizeX / float(image->getWidth());
		static float dy = sizeY / float(image->getHeight());

		return Vec3f(left + x * dx, bottom + y * dy, z).normalize();
	}


public:
	SimpleRayTracer(Scene* scene, Image* image) {
		this->scene = scene;
		this->image = image;
		this->camera = nullptr;
	}	
	SimpleRayTracer(Scene* scene, Image* image, Camera* camera) {
		this->scene = scene;
		this->image = image;
		this->camera = camera;
	}
	

	int searchClosestHit(const Ray& ray, HitRec& hitRec, Sphere *s =nullptr) {
		int count = 0;
		for (int i = 0; i < scene->spheres.size(); i++) {
			if (s != nullptr && (s == &scene->spheres[i])) continue;
			scene->spheres[i].hit(ray, hitRec,i);
			count++;
		}
		if (hitRec.anyHit)
			scene->spheres[hitRec.primIndex].computeSurfaceHitFields(ray, hitRec);
		return count;
	}

	void fireRays(void) {
		
		//bool hit = false;
		//ray.o = Vec3f(0.0f, 0.0f, 0.0f); //Set the start position of the eye rays to the origin
		//Set the start position of the eye rays to the origin
		
		#pragma omp parallel for num_threads(nthreads)
		for (int y = 0; y < image->getWidth(); y++) {
			Ray ray;
			HitRec hitRec;
			ray.o = camera->getPos();
			Vec3f bgColor = Vec3f(0.0f, 0.0f, 0.0f); //Blue
			Vec3f Color; //Default color is red
			for (int x = 0; x < image->getHeight(); x++) {
				ray.d = getEyeRayDirection(y, x);
				hitRec.anyHit = false;
				hitRec.tHit = std::numeric_limits<float>::infinity();
				interection_count += searchClosestHit(ray, hitRec);
				if (hitRec.anyHit) {
					Color = this->traceRay(ray, hitRec, 4, 0);											
					image->setPixel(y, x, Color);	

				}
				else {
					image->setPixel(y, x, bgColor);										
				}
			}
		}
		
		Vec3f Color;
		for (int x = 0; x < image->getHeight(); x++) {
			for (int y = 0; y < image->getWidth(); y++) {
				Color = image->getPixel(y, x);
				glSetPixel(y, x, Color);
			}
		}


		//glDrawPixels(
		//	image->getWidth(),
		//	image->getHeight(),
		//	GL_RGB,
		//	GL_FLOAT,
		//	image
		//);
		
		//glutSwapBuffers();
		//glEnd();

	}

	Vec3f traceRay(Ray& ray, HitRec& hitRec, int max_depth, int depth) {
		// ambient
		Vec3f Color(0.0f, 0.0f, 0.0f); 
		Vec3f RefColor(0.0f, 0.0f, 0.0f);
		Vec3f L, R, V, _L; 
		float N_L, R_V;
		Ray shadowRay;
		HitRec shadowHR;
		int num_of_lights = this->scene->lights.size();
		float kr = this->scene->spheres[hitRec.primIndex].reflection;
		for (int i = 0; i < num_of_lights; i++) {
			if (kr < 1.0f) {
				Color += this->scene->spheres[hitRec.primIndex].ambient * this->scene->lights[i].Ia;
				shadowRay.o = hitRec.p;
				shadowRay.d = (this->scene->lights[i].position - hitRec.p).normalize();
				shadowHR.anyHit = false;
				shadowHR.tHit = std::numeric_limits<float>::infinity(); // should be distance between light and hitpoint to avoid false shadows
				shadowRay.epsMoveStartAlongDir(); // move the hitpoint slightly above the sphere to enable self shadowing
				searchClosestHit(shadowRay, shadowHR); // intersection count
				if (!shadowHR.anyHit) {
					L = shadowRay.d;
					R = getReflectionVector(L, hitRec.n);
					V = (ray.o - hitRec.p).normalize();

					N_L = std::fmax(hitRec.n.dot(L), 0);
					R_V = std::fmax(R.dot(V), 0);

					Color += this->scene->lights[i].Id * this->scene->spheres[hitRec.primIndex].diffuse * N_L;
					Color += this->scene->lights[i].Is * this->scene->spheres[hitRec.primIndex].specular * pow(R_V, this->scene->spheres[hitRec.primIndex].shininess);
				}
			}			
			if ( kr>0.0f &&(max_depth - depth > 0)) {
				Ray refRay;
				Vec3f _D = ray.d * (-1.0f);
				refRay.d = getReflectionVector(_D, hitRec.n);
				refRay.o = hitRec.p;
				HitRec refHitRec;
				refHitRec.anyHit = false;
				refHitRec.tHit = std::numeric_limits<float>::infinity();  // should be distance between light and hitpoint to avoid false shadows
				searchClosestHit(refRay, refHitRec, &this->scene->spheres[hitRec.primIndex]); //intersection count
				if(refHitRec.anyHit)
					RefColor = traceRay(refRay, refHitRec, max_depth, depth + 1);
			}
						
		}
		return  Color* (1.0f - kr) + RefColor * kr;
	}
};


SimpleRayTracer* rayTracer;

void display(void) {
	//glBegin(GL_POINTS);
	glClear(GL_COLOR_BUFFER_BIT);
	//glRasterPos2i(0, 0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	auto start = std::chrono::high_resolution_clock::now();
	rayTracer->fireRays();
	auto finish = std::chrono::high_resolution_clock::now();
	
	std::cout << "TOTAL INTERSECTION TESTS =" << interection_count << endl;
	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(finish - start).count();
	std::cout << "RENDERING TIME=" << duration/1000.0f << std::endl;
	std::cout << "NUMBER OF THREADS = " << nthreads << std::endl;
	std::cout << "***********************" << std::endl;
	interection_count = 0;
	
	glFlush();
}

void changeSize(int w, int h) {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, w, 0, h);
	glViewport(0, 0, w, h);
}

void keypress(unsigned char key, int x, int y) {
		float camera_step = 0.5f;

		switch (key) {
			// CAMERA SETTINGS START
			case '1':
				nthreads = 1;
				break;
			case '2':
				nthreads = 2;
				break;
			case '3':
				nthreads = 3;
				break;
			case '4':
				nthreads = 4;
				break;
			case 'a':
				cam->adjustZPos(camera_step);
				break;
			case 'A':
				cam->adjustZPos(-camera_step);
				break;
			case 's':
				cam->adjustXPos(camera_step);
				break;
			case 'S':
				cam->adjustXPos(-camera_step);
				break;
			case 'd':
				cam->adjustYPos(camera_step);
				break;
			case 'D':
				cam->adjustYPos(-camera_step);
				break;
		}
		glutPostRedisplay();
}

void init(void)
{

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(640, 480);
	glutCreateWindow("SimpleRayTracer");
	glutDisplayFunc(display);
	glutReshapeFunc(changeSize);
	glutKeyboardFunc(keypress);

	glClearColor(0.0f, 1.0f, 0.0f, 1.0f);

	Scene* scene = new Scene;

	/* Make a sphere with radius of 3 */	
	Sphere s1(Vec3f(0.0f, 0.0f, -8.0f), 5.0f, Vec3f(0.05375f, 0.1f, 0.0625f), Vec3f(0.22525f, 0.5f, 0.22525f), Vec3f(0.22525f, 0.5f, 0.22525f), 8.0f, 0.6f);
	Sphere s2(Vec3f(3.0f, 3.0f, 0.0f), 1.0f, Vec3f(0.1f, 0.1f, 0.05375f), Vec3f(0.8f, 0.8f, 0.22525f), Vec3f(0.8f, 0.8f, 0.22525f), 8.0f, 0.05f); //yellow
	Sphere s5(Vec3f(1.5f, 1.5f, -2.5f), 0.5f, Vec3f(0.05375f, 0.1f, 0.1f), Vec3f(0.22525f, 0.8f, 0.8f), Vec3f(0.22525f, 0.8f, 0.8f), 8.0f, 0.3f); //light blue
	Sphere s3(Vec3f(-1.5f, -1.5f, -2.0f), 1.0f, Vec3f(0.1f, 0.06625f, 0.05375f), Vec3f(0.8f, 0.22525f, 0.22525f), Vec3f(0.8f, 0.22525f, 0.22525f), 8.0f, 0.2f); //red
	Sphere s4(Vec3f(1.5f, -1.5f, -2.0f), 1.0f, Vec3f(0.05375f, 0.06625f, 0.1f), Vec3f(0.22525f, 0.22525f, 0.8f), Vec3f(0.22525f, 0.22525f, 0.8f), 8.0f, 0.4f); //blue

	Sphere refSphere(Vec3f(3.0f, 0.0f, -2.0f), 1.0f, Vec3f(0.05375f, 0.06625f, 0.1f), Vec3f(0.22525f, 0.22525f, 0.8f), Vec3f(0.22525f, 0.22525f, 0.8f), 8.0f, 1.0f); //reflective
	Sphere refSphere2(Vec3f(0.0f, 0.0f, -2.0f), 1.0f, Vec3f(0.05375f, 0.06625f, 0.1f), Vec3f(0.22525f, 0.22525f, 0.8f), Vec3f(0.22525f, 0.22525f, 0.8f), 8.0f, 1.0f); //reflective
	Sphere refSphere3(Vec3f(0.0f, 2.2f, -2.0f), 1.0f, Vec3f(0.05375f, 0.06625f, 0.1f), Vec3f(0.22525f, 0.22525f, 0.8f), Vec3f(0.22525f, 0.22525f, 0.8f), 8.0f, 1.0f); //reflective
	
	Light L1(Vec3f(5.0f, 0.0f, 5.0f), Vec3f(0.2f, 0.2f, 0.2f), Vec3f(0.5f, 0.5f, 0.5f), Vec3f(0.6f, 0.6f, 0.6f));
	Light L3(Vec3f(5.0f, 5.0f, 5.0f), Vec3f(0.2f, 0.2f, 0.2f), Vec3f(0.5f, 0.5f, 0.5f), Vec3f(0.6f, 0.6f, 0.6f));


	Light L2(Vec3f(15.0f, 1.0f, 1.0f), Vec3f(0.2f, 0.2f, 0.2f), Vec3f(1.0f, 1.0f, 1.0f), Vec3f(0.2f, 0.2f, 0.2f));

	scene->add(s3);
	scene->add(s2);	
	scene->add(s4);	
	scene->add(s5);	
	scene->add(refSphere);	
	scene->add(refSphere2);	
	//scene->add(refSphere3);	
	scene->add(s1);

	scene->add(L1);
	scene->add(L3);

	Image* image = new Image(640, 480);

	rayTracer = new SimpleRayTracer(scene, image, cam);

}

void main(int argc, char** argv) {
	glutInit(&argc, argv);
	init();
	glutMainLoop();
	std::cout << "ERROR" << glGetError() << std::endl;
}
