#ifndef _SPHERE_H_
#define _SPHERE_H_

#include "Vec3.h"
#include "Ray.h"

class Sphere {
public:
	Vec3f c;
	float r;
	Vec3f color;
	Vec3f diffuse;
	Vec3f ambient;
	Vec3f specular;
	float shininess;
	float reflection;
public:
	Sphere(const Vec3f& cen, float rad, const Vec3f& col) : c(cen), r(rad), color(col) { }
	Sphere(const Vec3f& cen, float rad, const Vec3f& amb, const Vec3f& dif, const Vec3f& spec, float shininess, float ref) : c(cen), r(rad), ambient(amb),diffuse(dif),specular(spec),shininess(shininess), reflection(ref){ }

	bool hit(const Ray& r, HitRec& rec, int index) const;
	void computeSurfaceHitFields(const Ray& r, HitRec& rec) const;

};

	  

#endif